package com.vbalex.recolectadatos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface  EntradaDao {
    @Query("SELECT * FROM entradas")
    LiveData<List<Entrada>> getAll();

    @Query("SELECT * FROM entradas WHERE uid IN (:userUIds)")
    LiveData<List<Entrada>> loadAllByUIds(int[] userUIds);

    @Query("SELECT * FROM entradas WHERE id IN (:userIds)")
    LiveData<List<Entrada>> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM entradas WHERE id IN (:userId) LIMIT 1")
    Entrada loadAllById(int userId);

    @Query("SELECT * FROM entradas WHERE name LIKE :name LIMIT 1")
    Entrada findByName(String name);

    @Insert
    void insertAll(Entrada... entradas);

    @Insert
    void insert(Entrada entrada);

    @Delete
    void delete(Entrada entrada);
}
