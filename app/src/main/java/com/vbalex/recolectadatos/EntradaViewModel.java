package com.vbalex.recolectadatos;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class EntradaViewModel extends AndroidViewModel {

    private EntradaRepository mRepository;

    private LiveData<List<Entrada>> mAllEntradas;

    public EntradaViewModel (Application application) {
        super(application);
        mRepository = new EntradaRepository(application);
        mAllEntradas = mRepository.getAll();
    }

    LiveData<List<Entrada>> getAll() { return mAllEntradas; }

    public void insert(Entrada entrada) { mRepository.insert(entrada); }
}