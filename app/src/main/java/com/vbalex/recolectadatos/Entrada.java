package com.vbalex.recolectadatos;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "entradas")
public class Entrada {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "UId")
    public int uid;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "number")
    public int number;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "veiculos")
    public int veiculos;

    public Entrada(String name, int id, int number, String email, int veiculos){
        this.name = name;
        this.id = id;
        this.number = number;
        this.email = email;
        this.veiculos = veiculos;
    }
}