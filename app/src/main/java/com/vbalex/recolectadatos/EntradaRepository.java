package com.vbalex.recolectadatos;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class EntradaRepository {

    private EntradaDao mEntradaDao;
    private LiveData<List<Entrada>> mEntradas;

    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    EntradaRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        mEntradaDao = db.entradaDao();
        mEntradas = mEntradaDao.getAll();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    LiveData<List<Entrada>> getAll() {
        return mEntradas;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    void insert(final Entrada entrada) {
        AppDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                mEntradaDao.insert(entrada);
            }
        });
    }
}