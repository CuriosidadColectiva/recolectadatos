package com.vbalex.recolectadatos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Agregar extends AppCompatActivity {

    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_EMAIL = "mail";
    public static final String EXTRA_NUMBER = "number";
    public static final String EXTRA_CARS = "cars";

    private EditText mEditNameView;
    private EditText mEditIdView;
    private EditText mEditEmailView;
    private EditText mEditNumberView;
    private EditText mEditCarsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        mEditNameView = findViewById(R.id.edit_name);
        mEditIdView = findViewById(R.id.edit_id);
        mEditEmailView = findViewById(R.id.edit_email);
        mEditNumberView = findViewById(R.id.edit_number);
        mEditCarsView = findViewById(R.id.edit_cars);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditNameView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String name = mEditNameView.getText().toString();
                    String id = mEditIdView.getText().toString();
                    String email = mEditEmailView.getText().toString();
                    String number = mEditNumberView.getText().toString();
                    String cars = mEditCarsView.getText().toString();
                    replyIntent.putExtra(EXTRA_NAME, name);
                    replyIntent.putExtra(EXTRA_ID, id);
                    replyIntent.putExtra(EXTRA_EMAIL,email);
                    replyIntent.putExtra(EXTRA_NUMBER,number);
                    replyIntent.putExtra(EXTRA_CARS,cars);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
