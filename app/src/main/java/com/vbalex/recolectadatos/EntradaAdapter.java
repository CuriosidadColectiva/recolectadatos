package com.vbalex.recolectadatos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class EntradaAdapter extends RecyclerView.Adapter<EntradaAdapter.MyViewHolder> {
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,id,number,email,veiculos;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            id = (TextView) view.findViewById(R.id.id);
            number = (TextView) view.findViewById(R.id.number);
            email = (TextView) view.findViewById(R.id.email);
            veiculos = (TextView) view.findViewById(R.id.veiculos);
        }
    }

    private final LayoutInflater mInflater;
    private List<Entrada> entradaList;

    public EntradaAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.entrada_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (entradaList != null) {
            Entrada entrada = entradaList.get(position);
            holder.name.setText(entrada.name);
            holder.id.setText(entrada.id+"");
            holder.number.setText(entrada.number+"");
            holder.email.setText(entrada.email);
            holder.veiculos.setText(entrada.veiculos+"");
        } else {
            // Covers the case of data not being ready yet.
            holder.name.setText("No data");
        }
    }

    void setEntradas(List<Entrada> entradas){
        entradaList = entradas;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (entradaList != null)
            return entradaList.size();
        else return 0;
    }
}