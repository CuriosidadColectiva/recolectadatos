package com.vbalex.recolectadatos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    private EntradaViewModel mEntradaViewModel;

    public static final int NEW_ENTRADA_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = this.getApplicationContext();


        recyclerView = (RecyclerView) findViewById(R.id.main_list);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        final EntradaAdapter adapter = new EntradaAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mEntradaViewModel = new ViewModelProvider(this).get(EntradaViewModel.class);

        mEntradaViewModel.getAll().observe(this, new Observer<List<Entrada>>() {
            @Override
            public void onChanged(@Nullable final List<Entrada> entradas) {
                // Update the cached copy of the words in the adapter.
                adapter.setEntradas(entradas);
            }
        });


        FloatingActionButton fab = findViewById(R.id.add_icon);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Agregar.class);
                startActivityForResult(intent, NEW_ENTRADA_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_ENTRADA_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String name = data.getStringExtra(Agregar.EXTRA_NAME);
            String id = data.getStringExtra(Agregar.EXTRA_ID);
            String email = data.getStringExtra(Agregar.EXTRA_EMAIL);
            String number = data.getStringExtra(Agregar.EXTRA_NUMBER);
            String cars = data.getStringExtra(Agregar.EXTRA_CARS);

            Entrada entrada = new Entrada(name,Integer.parseInt(id),Integer.parseInt(number),email,Integer.parseInt(cars));
            mEntradaViewModel.insert(entrada);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
}

